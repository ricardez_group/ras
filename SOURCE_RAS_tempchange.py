# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 08:42:04 2022

@author: sskamali & g2patron
"""

'Import necessary packages and codes'

import time
import colorama
from colorama import Fore
import time as tm
from pyomo.environ import *
from pyomo.dae import * 
from pyomo.opt import SolverFactory
import numpy as np
import matplotlib.pyplot as plt
from PLANT_RAS_tempdist import PLANT_FISH # RAS plant model
from MPC_RAS_tempdist_git import MPC_FISH #RAS MPC model
from numpy.linalg import inv
import math

'Choose ipopt solver and change settings if necessary'

opt = SolverFactory('ipopt',tee=True)    
#opt.options['tol'] = 1E-15

'Define time parameters and lists'

Reltime=[] #list to save CPU time for each MPC solve
LMHW=50 #time periods to initialize the plant before engaging the MPC
h=0.1 # (days) time discretization finite-element length
P=50 # prediction/control horizon in terms of time steps
Tfinal=150 # (days) final simulation time 
Pfinal=int(Tfinal/h) # final simulation time in terms of time steps 

flag=1 #alternative time collocation schemes
if flag==0:
    n_coll=5
else:
    n_coll=10
    
'Define state initializations '

ny=69 #number of states

q1={0: 0.0004941262758492059, 1: 0.0026610418262743765, 2: 0.0005953489739768516, 3: 0.007999999921084513, 4: 0.008, 5: 0, 6: 0.0012704702721796658, 7: 0.0006551514887411368, 8: 0.024912867884266228, 9: 0.0004521422131039016, 10: 0.01048087596374192, 11: 0.0033743585516853034, 12: 0.021094991040364303, 13: 3.258529553051713e-05, 14: 0.2548137888637069, 15: 0.08203828436634969, 16: 0.002858920928472409, 17: 0.00032623896911251616, 18: 0.006184272269933473, 19: 0.019571519207748106, 20: 0.0004941262758492059, 21: 0.0026610418262743765, 22: 0.0005953489739768516, 23: 0.007999999921084513, 24: 0.008, 25: 0, 26: 0.0012704702721796658, 27: 0.0006551514887411368, 28: 0.024912867884266228, 29: 0.0004521422131039016, 30: 0.01048087596374192, 31: 0.0033743585516853034, 32: 0.021094991040364303, 33: 3.258529553051713e-05, 34: 0.2548137888637069, 35: 0.08203828436634969, 36: 0.002858920928472409, 37: 0.00032623896911251616, 38: 0.006184272269933473, 39: 0.019571519207748106, 40: 0.0006248485572695803, 41: 0.002697183223242683, 42: 0.0006262134488224627, 43: 0.008, 44: 0.008, 45: 0.001476084484174653, 46: 0.0012976268154275138, 47: 0.024912867884244745, 48: 0.0008619423141437846, 49: 0, 50: 3.2301907373349344e-05, 51: 0.0033450122867474297, 52: 0.010389725435038574, 53: 0.020911531222781592, 54: 0.04707215999973613, 55: 0.019415519999952287, 56: 0.009498240000433996, 57: 0.06460622112977724, 58: 0.19318526906403832, 59: 0.06460622112967648, 60: 0.12351189333605922, 61: 0.0002822708778531383, 62: 0.0002822708778531383, 63: 0.0002798160125124163, 64: 0.00047569827657743363, 65: 0.00047569827657743363, 66: 0.0004715612068880054, 67: 0.011565295475379671, 68: 0.011565295475379671, 69: 45}
States={0:0.00009,1:0.00247,2:0.00121,3:0.008,4:0.008,5:0,6:0.0004,7:0.00124,8:0.0001,9:0.00026,10:0.01,11:0.01,12:0.1,13:0.0003,14:0.1,15:0.1,16:0.00124,17:0.00121,18:0.00026,19:0.0003,   20:0.00009,21:0.00247,22:0.00121,23:0.008,24:0.008,25:0,26:0.0004,27:0.00124,28:0.0001,29:0.00026,30:0.01,31:0.01,32:0.1,33:0.0003,34:0.1,35:0.1,36:0.00124,37:0.00121,38:0.00026,39:0.0003,     40:0.00009,41:0.00247,42:0.00121,43:0.008,44:0.008,45:0.0004,46:0.00124,47:0.0001,48:0.00026,49:0,50:0.0003,51:0.001,52:0.001,53:0.1, 54:0,55:0,56:0,57:0,58:0,59:0,60:0,     61:0.0001,62:0.0001,63:0.0001,    64:0.001,65:0.001,66:0.001,67:0.001,68:0.001,    69:45, 70:2300}
TrueStates={0:0.00009,1:0.00247,2:0.00121,3:0.008,4:0.008,5:0,6:0.0004,7:0.00124,8:0.0001,9:0.00026,10:0.01,11:0.01,12:0.1,13:0.0003,14:0.1,15:0.1,16:0.00124,17:0.00121,18:0.00026,19:0.0003,   20:0.00009,21:0.00247,22:0.00121,23:0.008,24:0.008,25:0,26:0.0004,27:0.00124,28:0.0001,29:0.00026,30:0.01,31:0.01,32:0.1,33:0.0003,34:0.1,35:0.1,36:0.00124,37:0.00121,38:0.00026,39:0.0003,     40:0.00009,41:0.00247,42:0.00121,43:0.008,44:0.008,45:0.0004,46:0.00124,47:0.0001,48:0.00026,49:0,50:0.0003,51:0.001,52:0.001,53:0.1, 54:0,55:0,56:0,57:0,58:0,59:0,60:0,     61:0.0001,62:0.0001,63:0.0001,    64:0.001,65:0.001,66:0.001,67:0.001,68:0.001,    69:45, 70:2300}
InitMPCStates={0:0.00009,1:0.00247,2:0.00121,3:0.008,4:0.008,5:0,6:0.0004,7:0.00124,8:0.0001,9:0.00026,10:0.01,11:0.01,12:0.1,13:0.0003,14:0.1,15:0.1,16:0.00124,17:0.00121,18:0.00026,19:0.0003,   20:0.00009,21:0.00247,22:0.00121,23:0.008,24:0.008,25:0,26:0.0004,27:0.00124,28:0.0001,29:0.00026,30:0.01,31:0.01,32:0.1,33:0.0003,34:0.1,35:0.1,36:0.00124,37:0.00121,38:0.00026,39:0.0003,     40:0.00009,41:0.00247,42:0.00121,43:0.008,44:0.008,45:0.0004,46:0.00124,47:0.0001,48:0.00026,49:0,50:0.0003,51:0.001,52:0.001,53:0.1, 54:0,55:0,56:0,57:0,58:0,59:0,60:0,     61:0.0001,62:0.0001,63:0.0001,    64:0.001,65:0.001,66:0.001,67:0.001,68:0.001,    69:45, 70:2300}

'Define input initializations'
   
inputs33={1: 0.1975588767167534, 2: 0.4057566892100667 , 3:3.0319976249006744 , 4: 0.4057566892100667}

Feed=2
Frate={1:Feed}

'Create lists for storing data'

ty=[] # time elapsed (days)
y=[] # plant state
y_MPC=[] # MPC-predicted state
Simulation=[] # plant state excluding economics
u1=[] # Fresh oxygen flowrate
u2=[] # Aeration rate 1
u3=[] # Water makeup flowrate
u4=[] # Feed rate
u5=[] # Aeration rate 2
uu1=[] # Fresh oxygen flowrate
uu2=[] # Aeration rate 1
uu3=[] # Water makeup flowrate
uu4=[] # Feed rate
uu5=[] # Aeration rate 2

for i in range(74):
    y.append([])
    y_MPC.append([])

'Making history of horizon length'

u1.append(value(inputs33[1]))
u2.append(value(inputs33[2]))
u3.append(value(inputs33[3]))
u5.append(value(inputs33[4]))
u4.append(value(Frate[1]))

'Initialize plant by running with constant inputs for LMHW intervals'

for i in range (LMHW):
    
    'Add noise to each state'
    
    nnx0=nx[0][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx1=nx[1][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx2=nx[2][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx3=nx[3][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx4=nx[4][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx5=nx[5][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx6=nx[6][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx7=nx[7][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx8=nx[8][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx9=nx[9][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx10=nx[10][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx11=nx[11][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx12=nx[12][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx13=nx[13][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx14=nx[14][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx15=nx[15][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx16=nx[16][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx17=nx[17][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx18=nx[18][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx19=nx[19][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx20=nx[20][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx21=nx[21][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx22=nx[22][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx23=nx[23][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx24=nx[24][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx25=nx[25][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx26=nx[26][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx27=nx[27][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx28=nx[28][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx29=nx[29][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx30=nx[30][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx31=nx[31][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx32=nx[32][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx33=nx[33][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx34=nx[34][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx35=nx[35][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx36=nx[36][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx37=nx[37][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx38=nx[38][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx39=nx[39][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx40=nx[40][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx41=nx[41][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx42=nx[42][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx43=nx[43][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx44=nx[44][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx45=nx[45][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx46=nx[46][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx47=nx[47][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx48=nx[48][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx49=nx[49][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx50=nx[50][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx51=nx[51][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx52=nx[52][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx53=nx[53][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx54=nx[54][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx55=nx[55][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx56=nx[56][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx57=nx[57][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx58=nx[58][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx59=nx[59][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx60=nx[60][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx61=nx[61][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx62=nx[62][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx63=nx[63][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx64=nx[64][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx65=nx[65][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx66=nx[66][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx67=nx[67][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx68=nx[68][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx69=nx[69][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    
    'Simulate plant by running a dynamic feasibility problem'    

    plant=PLANT_FISH(h,States,inputs33,n_coll,flag,nnx0,nnx1,nnx2,nnx3,nnx4,nnx5,nnx6,nnx7,nnx8,nnx9,nnx10,nnx11,nnx12,nnx13,nnx14,nnx15,nnx16,nnx17,nnx18,nnx19,nnx20,nnx21,nnx22,nnx23,nnx24,nnx25,nnx26,nnx27,nnx28,nnx29,nnx30,nnx31,nnx32,nnx33,nnx34,nnx35,nnx36,nnx37,nnx38,nnx39,nnx40,nnx41,nnx42,nnx43,nnx44,nnx45,nnx46,nnx47,nnx48,nnx49,nnx50,nnx51,nnx52,nnx53,nnx54,nnx55,nnx56,nnx57,nnx58,nnx59,nnx60,nnx61,nnx62,nnx63,nnx64,nnx65,nnx66,nnx67,nnx68,nnx69,Frate)
    plant.T = value(15.5) #set plant temperature
    opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt") #choose ipopt solver                                         
    opt.options['linear_solver']='mumps' #choose mumps 
    results = opt.solve(plant,tee=True) #solve the problem

    'Saving time step results to lists for later plotting'
    
    ty.append(i*h)
    y[0].append(value(plant.z0[h]))  
    y[1].append(value(plant.z1[h]))  
    y[2].append(value(plant.z2[h]))  
    y[3].append(value(plant.z3[h]))  
    y[4].append(value(plant.z4))  
    y[5].append(value(plant.z5))  
    y[6].append(value(plant.z6[h]))  
    y[7].append(value(plant.z7[h]))  
    y[8].append(value(plant.z8[h]))  
    y[9].append(value(plant.z9[h]))  
    y[10].append(value(plant.z10[h]))  
    y[11].append(value(plant.z11[h]))  
    y[12].append(value(plant.z12[h]))  
    y[13].append(value(plant.z13[h]))  
    y[14].append(value(plant.z14[h]))  
    y[15].append(value(plant.z15[h]))  
    y[16].append(value(plant.z16[h]))  
    y[17].append(value(plant.z17[h]))  
    y[18].append(value(plant.z18[h]))  
    y[19].append(value(plant.z19[h]))  
    y[20].append(value(plant.z20[h]))  
    y[21].append(value(plant.z21[h]))  
    y[22].append(value(plant.z22[h]))  
    y[23].append(value(plant.z23[h]))  
    y[24].append(value(plant.z24))  
    y[25].append(value(plant.z25))  
    y[26].append(value(plant.z26[h]))  
    y[27].append(value(plant.z27[h]))  
    y[28].append(value(plant.z28[h]))  
    y[29].append(value(plant.z29[h]))  
    y[30].append(value(plant.z30[h]))  
    y[31].append(value(plant.z31[h]))  
    y[32].append(value(plant.z32[h]))  
    y[33].append(value(plant.z33[h]))  
    y[34].append(value(plant.z34[h]))  
    y[35].append(value(plant.z35[h]))  
    y[36].append(value(plant.z36[h]))  
    y[37].append(value(plant.z37[h]))  
    y[38].append(value(plant.z38[h]))  
    y[39].append(value(plant.z39[h])) 
    y[40].append(value(plant.z40[h]))  
    y[41].append(value(plant.z41[h]))  
    y[42].append(value(plant.z42[h]))  
    y[43].append(value(plant.z43[h]))  
    y[44].append(value(plant.z44))  
    y[45].append(value(plant.z45[h]))  
    y[46].append(value(plant.z46[h]))  
    y[47].append(value(plant.z47[h]))  
    y[48].append(value(plant.z48[h]))  
    y[49].append(value(plant.z49))  
    y[50].append(value(plant.z50[h]))  
    y[51].append(value(plant.z51[h]))  
    y[52].append(value(plant.z52[h]))  
    y[53].append(value(plant.z53[h]))  
    y[54].append(value(plant.z54[h]))  
    y[55].append(value(plant.z55[h]))  
    y[56].append(value(plant.z56[h]))  
    y[57].append(value(plant.z57[h]))  
    y[58].append(value(plant.z58[h]))  
    y[59].append(value(plant.z59[h]))  
    y[60].append(value(plant.z60[h]))  
    y[61].append(value(plant.z61[h]))  
    y[62].append(value(plant.z62[h]))  
    y[63].append(value(plant.z63[h]))  
    y[64].append(value(plant.z64[h]))  
    y[65].append(value(plant.z65[h]))  
    y[66].append(value(plant.z66[h]))  
    y[67].append(value(plant.z67[h]))  
    y[68].append(value(plant.z68[h]))  
    y[69].append(value(plant.z69[h]))  
    y[70].append(value(plant.z70[h]))  
    y[71].append(value(7.15*plant.z69[h]*plant.z70[h]/1000))            
    y[72].append(value(plant.kl*0.04*0.1 + 1.55*plant.F*0.1))
    if i == 0:
        y[73].append(value(0))             
    else:
        y[73].append(value(y[71][-1] - y[72][-1]*h + y[73][-1]))     

    'Saving time step results to lists for MPC initialization (InitMPCStates) and initial conditions (States)'

    for i in range(0,71):
        States[i] = value(y[i][-1])
        InitMPCStates[i] = value(y[i][-1]) 

    'Save inputs to list to use later'
 
    u1.append(value(inputs33[1]))
    u2.append(value(inputs33[2]))
    u3.append(value(inputs33[3]))
    u5.append(value(inputs33[4]))
    u4.append(value( Frate[1]))

    uu1.append(value(inputs33[1]))
    uu2.append(value(inputs33[2]))
    uu3.append(value(inputs33[3]))
    uu5.append(value(inputs33[4]))
    uu4.append(value(Frate[1]))
    
print(Fore.BLUE + 'End of initialization') # announcing end of initialization


time=[]
for i in range (LMHW,Pfinal+1):
    start1=tm.time() #saving iteration start time 
    time.append(i*h) #saving time instance (days)
    print(Fore.GREEN + str(i)) # printing integer time instance
    print(Fore.RED + 'MPC starting') #printing announcement that MPC solve is starting

    'Set points if using tracking controller'

    ref={1:0.0004941262758492059, 2: 0.0006248485572695803, 3: 0.007999999921084513, 4:0.007999999922041812, 5: 0.021094991040364303, 6: 0.020911531222781592}

    InitMPCStates[70] = States[70]
    InitMPCStates[3] = States[3]
    InitMPCStates[23] = States[23]
    InitMPCStates[43] = States[43]
    
    'Solve MPC'
    
    mpc=MPC_FISH(h,P,InitMPCStates,inputs33,n_coll,flag,ref,Frate)
    mpc.T = value(15.5) #Set measurable internal model temperature
    mpc.kl[0].fix(u1[-1]) #Set first input instance in MPC to the input from the previous iteration
    mpc.klBR1[0].fix(u2[-1]) #Set first input instance in MPC to the input from the previous iteration
    mpc.Q4[0].fix(u3[-1]) #Set first input instance in MPC to the input from the previous iteration
    mpc.klBR2[0].fix(u5[-1]) #Set first input instance in MPC to the input from the previous iteration
    mpc.F[0].fix(u4[-1]) #Set first input instance in MPC to the input from the previous iteration
#    if i < 300: #impose temperature (or any other variable) disturbance after a given time
#        pass
#    else:
#        mpc.T = value(15)
    results = opt.solve(mpc,tee=True) #solve MPC

    'Save MPC-determined manipulated variables'
    
    inputs33[1]=mpc.kl[h].value
    u1.append(value(mpc.kl[h]))
    uu1.append(value(mpc.kl[h]))

    inputs33[2]=mpc.klBR1[h].value
    u2.append(value(mpc.klBR1[h]))
    uu2.append(value(mpc.klBR1[h]))
    
    inputs33[3]=mpc.Q4[h].value
    u3.append(value(mpc.Q4[h]))
    uu3.append(value(mpc.Q4[h]))

    Frate[1]=value(mpc.F[h])
    u4.append(value(mpc.F[h]))
    uu4.append(value(mpc.F[h]))
    
    inputs33[4]=mpc.klBR2[h].value
    u5.append(value(mpc.klBR2[h]))
    uu5.append(value(mpc.klBR2[h]))
        
    'Saving MPC-predicted time step results'           
    
    y_MPC[0].append(value(mpc.z0[h]))  
    y_MPC[1].append(value(mpc.z1[h]))  
    y_MPC[2].append(value(mpc.z2[h]))  
    y_MPC[3].append(value(mpc.z3[h]))  
    y_MPC[4].append(value(mpc.z4))  
    y_MPC[5].append(value(mpc.z5))  
    y_MPC[6].append(value(mpc.z6[h]))  
    y_MPC[7].append(value(mpc.z7[h]))  
    y_MPC[8].append(value(mpc.z8[h]))  
    y_MPC[9].append(value(mpc.z9[h]))  
    y_MPC[10].append(value(mpc.z10[h]))  
    y_MPC[11].append(value(mpc.z11[h]))  
    y_MPC[12].append(value(mpc.z12[h]))  
    y_MPC[13].append(value(mpc.z13[h]))  
    y_MPC[14].append(value(mpc.z14[h]))  
    y_MPC[15].append(value(mpc.z15[h]))  
    y_MPC[16].append(value(mpc.z16[h]))  
    y_MPC[17].append(value(mpc.z17[h]))  
    y_MPC[18].append(value(mpc.z18[h]))  
    y_MPC[19].append(value(mpc.z19[h]))  
    y_MPC[20].append(value(mpc.z20[h]))  
    y_MPC[21].append(value(mpc.z21[h]))  
    y_MPC[22].append(value(mpc.z22[h]))  
    y_MPC[23].append(value(mpc.z23[h]))  
    y_MPC[24].append(value(mpc.z24))  
    y_MPC[25].append(value(mpc.z25))  
    y_MPC[26].append(value(mpc.z26[h]))  
    y_MPC[27].append(value(mpc.z27[h]))  
    y_MPC[28].append(value(mpc.z28[h]))  
    y_MPC[29].append(value(mpc.z29[h]))  
    y_MPC[30].append(value(mpc.z30[h]))  
    y_MPC[31].append(value(mpc.z31[h]))  
    y_MPC[32].append(value(mpc.z32[h]))  
    y_MPC[33].append(value(mpc.z33[h]))  
    y_MPC[34].append(value(mpc.z34[h]))  
    y_MPC[35].append(value(mpc.z35[h]))  
    y_MPC[36].append(value(mpc.z36[h]))  
    y_MPC[37].append(value(mpc.z37[h]))  
    y_MPC[38].append(value(mpc.z38[h]))  
    y_MPC[39].append(value(mpc.z39[h])) 
    y_MPC[40].append(value(mpc.z40[h]))  
    y_MPC[41].append(value(mpc.z41[h]))  
    y_MPC[42].append(value(mpc.z42[h]))  
    y_MPC[43].append(value(mpc.z43[h]))  
    y_MPC[44].append(value(mpc.z44))  
    y_MPC[45].append(value(mpc.z45[h]))  
    y_MPC[46].append(value(mpc.z46[h]))  
    y_MPC[47].append(value(mpc.z47[h]))  
    y_MPC[48].append(value(mpc.z48[h]))  
    y_MPC[49].append(value(mpc.z49))  
    y_MPC[50].append(value(mpc.z50[h]))  
    y_MPC[51].append(value(mpc.z51[h]))  
    y_MPC[52].append(value(mpc.z52[h]))  
    y_MPC[53].append(value(mpc.z53[h]))  
    y_MPC[54].append(value(mpc.z54[h]))  
    y_MPC[55].append(value(mpc.z55[h]))  
    y_MPC[56].append(value(mpc.z56[h]))  
    y_MPC[57].append(value(mpc.z57[h]))  
    y_MPC[58].append(value(mpc.z58[h]))  
    y_MPC[59].append(value(mpc.z59[h]))  
    y_MPC[60].append(value(mpc.z60[h]))  
    y_MPC[61].append(value(mpc.z61[h]))  
    y_MPC[62].append(value(mpc.z62[h]))  
    y_MPC[63].append(value(mpc.z63[h]))  
    y_MPC[64].append(value(mpc.z64[h]))  
    y_MPC[65].append(value(mpc.z65[h]))  
    y_MPC[66].append(value(mpc.z66[h]))  
    y_MPC[67].append(value(mpc.z67[h]))  
    y_MPC[68].append(value(mpc.z68[h]))  
    y_MPC[69].append(value(mpc.z69[h]))  
    y_MPC[70].append(value(mpc.z70[h]))  
    
    
    'Add new noise'
    
    nnx0=nx[0][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx1=nx[1][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx2=nx[2][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx3=nx[3][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx4=nx[4][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx5=nx[5][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx6=nx[6][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx7=nx[7][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx8=nx[8][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx9=nx[9][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx10=nx[10][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx11=nx[11][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx12=nx[12][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx13=nx[13][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx14=nx[14][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx15=nx[15][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx16=nx[16][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx17=nx[17][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx18=nx[18][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx19=nx[19][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx20=nx[20][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx21=nx[21][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx22=nx[22][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx23=nx[23][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx24=nx[24][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx25=nx[25][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx26=nx[26][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx27=nx[27][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx28=nx[28][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx29=nx[29][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx30=nx[30][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx31=nx[31][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx32=nx[32][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx33=nx[33][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx34=nx[34][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx35=nx[35][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx36=nx[36][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx37=nx[37][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx38=nx[38][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx39=nx[39][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx40=nx[40][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx41=nx[41][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx42=nx[42][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx43=nx[43][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx44=nx[44][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx45=nx[45][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx46=nx[46][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx47=nx[47][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx48=nx[48][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx49=nx[49][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx50=nx[50][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx51=nx[51][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx52=nx[52][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx53=nx[53][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx54=nx[54][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx55=nx[55][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx56=nx[56][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx57=nx[57][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx58=nx[58][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx59=nx[59][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx60=nx[60][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx61=nx[61][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx62=nx[62][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx63=nx[63][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx64=nx[64][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx65=nx[65][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx66=nx[66][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx67=nx[67][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx68=nx[68][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    nnx69=nx[69][i*int((h*n_coll)):(i+1)*int((h*n_coll))+1]
    
    'Simulating plant'
    
    print(Fore.GREEN + 'Plant start')
    plant=PLANT_FISH(h,States,inputs33,n_coll,flag,nnx0,nnx1,nnx2,nnx3,nnx4,nnx5,nnx6,nnx7,nnx8,nnx9,nnx10,nnx11,nnx12,nnx13,nnx14,nnx15,nnx16,nnx17,nnx18,nnx19,nnx20,nnx21,nnx22,nnx23,nnx24,nnx25,nnx26,nnx27,nnx28,nnx29,nnx30,nnx31,nnx32,nnx33,nnx34,nnx35,nnx36,nnx37,nnx38,nnx39,nnx40,nnx41,nnx42,nnx43,nnx44,nnx45,nnx46,nnx47,nnx48,nnx49,nnx50,nnx51,nnx52,nnx53,nnx54,nnx55,nnx56,nnx57,nnx58,nnx59,nnx60,nnx61,nnx62,nnx63,nnx64,nnx65,nnx66,nnx67,nnx68,nnx69,Frate)
    plant.T = value(15.5) #Set plant temperature
#    if i < 300: #induce disturbance
#        pass
#    else:
#        plant.T = value(15)
    results = opt.solve(plant,tee=True) #solve plany
 
    'Hold state constant of plant doesnt solve to optimality; this rarely occurs'
    'Else, save time step results to lists for later plotting'
    
    if (results.solver.termination_condition != TerminationCondition.optimal):
        for j in range(9,len(y)):
            y[j].append(y[j][-1])
    else:    
        y[0].append(value(plant.z0[h]))  
        y[1].append(value(plant.z1[h]))  
        y[2].append(value(plant.z2[h]))  
        y[3].append(value(plant.z3[h]))  
        y[4].append(value(plant.z4))  
        y[5].append(value(plant.z5))  
        y[6].append(value(plant.z6[h]))  
        y[7].append(value(plant.z7[h]))  
        y[8].append(value(plant.z8[h]))  
        y[9].append(value(plant.z9[h]))  
        y[10].append(value(plant.z10[h]))  
        y[11].append(value(plant.z11[h]))  
        y[12].append(value(plant.z12[h]))  
        y[13].append(value(plant.z13[h]))  
        y[14].append(value(plant.z14[h]))  
        y[15].append(value(plant.z15[h]))  
        y[16].append(value(plant.z16[h]))  
        y[17].append(value(plant.z17[h]))  
        y[18].append(value(plant.z18[h]))  
        y[19].append(value(plant.z19[h]))  
        y[20].append(value(plant.z20[h]))  
        y[21].append(value(plant.z21[h]))  
        y[22].append(value(plant.z22[h]))  
        y[23].append(value(plant.z23[h]))  
        y[24].append(value(plant.z24))  
        y[25].append(value(plant.z25))  
        y[26].append(value(plant.z26[h]))  
        y[27].append(value(plant.z27[h]))  
        y[28].append(value(plant.z28[h]))  
        y[29].append(value(plant.z29[h]))  
        y[30].append(value(plant.z30[h]))  
        y[31].append(value(plant.z31[h]))  
        y[32].append(value(plant.z32[h]))  
        y[33].append(value(plant.z33[h]))  
        y[34].append(value(plant.z34[h]))  
        y[35].append(value(plant.z35[h]))  
        y[36].append(value(plant.z36[h]))  
        y[37].append(value(plant.z37[h]))  
        y[38].append(value(plant.z38[h]))  
        y[39].append(value(plant.z39[h])) 
        y[40].append(value(plant.z40[h]))  
        y[41].append(value(plant.z41[h]))  
        y[42].append(value(plant.z42[h]))  
        y[43].append(value(plant.z43[h]))  
        y[44].append(value(plant.z44))  
        y[45].append(value(plant.z45[h]))  
        y[46].append(value(plant.z46[h]))  
        y[47].append(value(plant.z47[h]))  
        y[48].append(value(plant.z48[h]))  
        y[49].append(value(plant.z49))  
        y[50].append(value(plant.z50[h]))  
        y[51].append(value(plant.z51[h]))  
        y[52].append(value(plant.z52[h]))  
        y[53].append(value(plant.z53[h]))  
        y[54].append(value(plant.z54[h]))  
        y[55].append(value(plant.z55[h]))  
        y[56].append(value(plant.z56[h]))  
        y[57].append(value(plant.z57[h]))  
        y[58].append(value(plant.z58[h]))  
        y[59].append(value(plant.z59[h]))  
        y[60].append(value(plant.z60[h]))  
        y[61].append(value(plant.z61[h]))  
        y[62].append(value(plant.z62[h]))  
        y[63].append(value(plant.z63[h]))  
        y[64].append(value(plant.z64[h]))  
        y[65].append(value(plant.z65[h]))  
        y[66].append(value(plant.z66[h]))  
        y[67].append(value(plant.z67[h]))  
        y[68].append(value(plant.z68[h]))  
        y[69].append(value(plant.z69[h]))  
        y[70].append(value(plant.z70[h]))  
        y[71].append(value(7.15*plant.z69[h]*plant.z70[h]/1000))            
        y[72].append(value(plant.kl*0.04*0.1 + 1.55*plant.F*0.1))
        y[73].append(value(y[71][-1] - y[72][-1]*h + y[73][-1]))    
    
    'Saving time step results for initial conditions in next time step'

    for i in range(0,71):
        States[i] = value(y[i][-1])
    
    'Maintain  horizon-length input vectors'    
       
    del u1[0]
    del u2[0]
    del u3[0]
    del u4[0]
    del u5[0]
    
    'Calculate time to simulate plant and execute controller for the time instance '

    start2=tm.time() #end time of iteration 
    differ=start2-start1 #difference between start and end times
    Reltime.append(differ) #add to time list

'Calculate cumulative profits and utility costs'

CP = []
CU = []
for i in range(0,len(y[43])):
    if i == 0:
        CU.append(0)
        CP.append(0)
    else:
        CU.append(y[72][i]+CU[i-1])
        CP.append((y[71][i]-CU[i]))

'Plot result'

fig,ax = plt.subplots(4,2, figsize=(11,7),dpi=100,sharex=True)

ax[0,0].plot(time,uu1[:-LMHW],'g')
ax[0,0].set_title('Fresh oxygen flowrate')
ax[1,0].plot(time,uu2[:-LMHW],'g')
ax[1,0].set_title('Aeration rate (1)')
ax[2,0].plot(time,uu5[:-LMHW],'g')
ax[2,0].set_title('Aeration rate (2)')
ax[3,0].plot(time,uu4[:-LMHW],'g')
ax[3,0].set_title('Feed flowrate')

ax[0,1].plot(time,uu3[:-LMHW],'g')
ax[0,1].set_title('Water makeup flowrate')
ax[1,1].plot(time,y[69][:-LMHW],'g')
ax[1,1].set_title('Fish weight')
ax[2,1].plot(time,y[70][:-LMHW],'g')
ax[2,1].set_title('Population')
ax[3,1].plot(time[1:],CP[1:-LMHW],'g')
ax[3,1].set_title('Profit')

'Find maximum profit and time at which it occurs'

max(CP)
time[CP.index(max(CP))]