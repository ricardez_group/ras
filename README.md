This repository contains the code for the paper "Economically optimal operation of recirculating aquaculture systems under uncertainty" by Gabriel Patrón and Luis Ricardez-Sandoval.

The scenario_A_data.spydata and plot_data.py files allow for plotting of a typical RAS scenario without having to redo the timely simulation. Simply import the .spydata into a Spyder console and run the .py file.

To run the MPC-operated RAS simulation, follow these instructions:

    1) Download the SOURCE_RAS_tempchange.py, additionale_dy.py, MPC_RAS_tempdist_git.py, and PLANT_RAS_tempdist.py files into the same folder.    
    2) Run the additionale_dy.py file to import noise into the simulation. 
    3) Run SOURCE_RAS_tempchange.py file; this will begin the simulation, which takes a few days to run.

All files can be run as displayed herein. They require installation of the Pyomo modelling environment (http://www.pyomo.org/) and the IPOPT solver (https://www.coin-or.org/download/binary/Ipopt/).
