# -*- coding: utf-8 -*-
"""
Created on Mon May  9 08:43:44 2022

@author: sskamali & g2patron
"""

'Needs to run prior to starting simulation; this code imports noise for an excel file'

import pandas as pd
import numpy as np



df=pd.read_excel('C:/Users/g2patron/Desktop/noiseM6P6_new.xlsx')

nx=[]
n0x=[]
df = df.astype(float)

nx_0=df.iloc[:,0]
nx_1=df.iloc[:,1]
nx_2=df.iloc[:,2]
nx_3=df.iloc[:,3]
nx_4=df.iloc[:,4]
nx_5=df.iloc[:,5]
nx_6=df.iloc[:,6]
nx_7=df.iloc[:,7]
nx_8=df.iloc[:,8]
nx_9=df.iloc[:,9]
nx_10=df.iloc[:,10]
nx_11=df.iloc[:,11]
nx_12=df.iloc[:,12]
nx_13=df.iloc[:,13]
nx_14=df.iloc[:,14]
nx_15=df.iloc[:,15]
nx_16=df.iloc[:,16]
nx_17=df.iloc[:,17]
nx_18=df.iloc[:,18]
nx_19=df.iloc[:,19]
nx_20=df.iloc[:,20]
nx_21=df.iloc[:,21]
nx_22=df.iloc[:,22]
nx_23=df.iloc[:,23]
nx_24=df.iloc[:,24]
nx_25=df.iloc[:,25]
nx_26=df.iloc[:,26]
nx_27=df.iloc[:,27]
nx_28=df.iloc[:,28]
nx_29=df.iloc[:,29]
nx_30=df.iloc[:,30]
nx_31=df.iloc[:,31]
nx_32=df.iloc[:,32]
nx_33=df.iloc[:,33]
nx_34=df.iloc[:,34]
nx_35=df.iloc[:,35]
nx_36=df.iloc[:,36]
nx_37=df.iloc[:,37]
nx_38=df.iloc[:,38]
nx_39=df.iloc[:,39]
nx_40=df.iloc[:,40]
nx_41=df.iloc[:,41]
nx_42=df.iloc[:,42]
nx_43=df.iloc[:,43]
nx_44=df.iloc[:,44]
nx_45=df.iloc[:,45]
nx_46=df.iloc[:,46]
nx_47=df.iloc[:,47]
nx_48=df.iloc[:,48]
nx_49=df.iloc[:,49]
nx_50=df.iloc[:,50]
nx_51=df.iloc[:,51]
nx_52=df.iloc[:,52]
nx_53=df.iloc[:,53]
nx_54=df.iloc[:,54]
nx_55=df.iloc[:,55]
nx_56=df.iloc[:,56]
nx_57=df.iloc[:,57]
nx_58=df.iloc[:,58]
nx_59=df.iloc[:,59]
nx_60=df.iloc[:,60]
nx_61=df.iloc[:,61]
nx_62=df.iloc[:,62]
nx_63=df.iloc[:,63]
nx_64=df.iloc[:,64]
nx_65=df.iloc[:,65]
nx_66=df.iloc[:,66]
nx_67=df.iloc[:,67]
nx_68=df.iloc[:,68]
nx_69=df.iloc[:,69]



n0x_0=df.iloc[:,70]
n0x_1=df.iloc[:,71]
n0x_2=df.iloc[:,72]
n0x_3=df.iloc[:,73]
n0x_4=df.iloc[:,74]
n0x_5=df.iloc[:,75]
n0x_6=df.iloc[:,76]
n0x_7=df.iloc[:,77]
n0x_8=df.iloc[:,78]
n0x_9=df.iloc[:,79]
n0x_10=df.iloc[:,80]
n0x_11=df.iloc[:,81]
n0x_12=df.iloc[:,82]
n0x_13=df.iloc[:,83]
n0x_14=df.iloc[:,84]
n0x_15=df.iloc[:,85]
n0x_16=df.iloc[:,86]
n0x_17=df.iloc[:,87]
n0x_18=df.iloc[:,88]
n0x_19=df.iloc[:,89]
n0x_20=df.iloc[:,90]
n0x_21=df.iloc[:,91]
n0x_22=df.iloc[:,92]
n0x_23=df.iloc[:,93]
n0x_24=df.iloc[:,94]
n0x_25=df.iloc[:,95]
n0x_26=df.iloc[:,96]
n0x_27=df.iloc[:,97]
n0x_28=df.iloc[:,98]
n0x_29=df.iloc[:,99]
n0x_30=df.iloc[:,100]
n0x_31=df.iloc[:,101]
n0x_32=df.iloc[:,102]
n0x_33=df.iloc[:,103]
n0x_34=df.iloc[:,104]
n0x_35=df.iloc[:,105]
n0x_36=df.iloc[:,106]
n0x_37=df.iloc[:,107]
n0x_38=df.iloc[:,108]
n0x_39=df.iloc[:,109]
n0x_40=df.iloc[:,110]
n0x_41=df.iloc[:,111]
n0x_42=df.iloc[:,112]
n0x_43=df.iloc[:,113]
n0x_44=df.iloc[:,114]
n0x_45=df.iloc[:,115]
n0x_46=df.iloc[:,116]
n0x_47=df.iloc[:,117]
n0x_48=df.iloc[:,118]
n0x_49=df.iloc[:,119]
n0x_50=df.iloc[:,120]
n0x_51=df.iloc[:,121]
n0x_52=df.iloc[:,122]
n0x_53=df.iloc[:,123]
n0x_54=df.iloc[:,124]
n0x_55=df.iloc[:,125]
n0x_56=df.iloc[:,126]
n0x_57=df.iloc[:,127]
n0x_58=df.iloc[:,128]
n0x_59=df.iloc[:,129]
n0x_60=df.iloc[:,130]
n0x_61=df.iloc[:,131]
n0x_62=df.iloc[:,132]
n0x_63=df.iloc[:,133]
n0x_64=df.iloc[:,134]
n0x_65=df.iloc[:,135]
n0x_66=df.iloc[:,136]
n0x_67=df.iloc[:,137]
n0x_68=df.iloc[:,138]
n0x_69=df.iloc[:,139]





nx_0=np.array(nx_0, dtype = float)
n0x_0=np.array(n0x_0, dtype = float)
nx.append(nx_0)
n0x.append(n0x_0)
nx_1=np.array(nx_1, dtype = float)
n0x_1=np.array(n0x_1, dtype = float)
nx.append(nx_1)
n0x.append(n0x_1)
nx_2=np.array(nx_2, dtype = float)
n0x_2=np.array(n0x_2, dtype = float)
nx.append(nx_2)
n0x.append(n0x_2)
nx_3=np.array(nx_3, dtype = float)
n0x_3=np.array(n0x_3, dtype = float)
nx.append(nx_3)
n0x.append(n0x_3)
nx_4=np.array(nx_4, dtype = float)
n0x_4=np.array(n0x_4, dtype = float)
nx.append(nx_4)
n0x.append(n0x_4)
nx_5=np.array(nx_5, dtype = float)
n0x_5=np.array(n0x_5, dtype = float)
nx.append(nx_5)
n0x.append(n0x_5)
nx_6=np.array(nx_6, dtype = float)
n0x_6=np.array(n0x_6, dtype = float)
nx.append(nx_6)
n0x.append(n0x_6)
nx_7=np.array(nx_7, dtype = float)
n0x_7=np.array(n0x_7, dtype = float)
nx.append(nx_7)
n0x.append(n0x_7)
nx_8=np.array(nx_8, dtype = float)
n0x_8=np.array(n0x_8, dtype = float)
nx.append(nx_8)
n0x.append(n0x_8)
nx_9=np.array(nx_9, dtype = float)
n0x_9=np.array(n0x_9, dtype = float)
nx.append(nx_9)
n0x.append(n0x_9)
nx_10=np.array(nx_10, dtype = float)
n0x_10=np.array(n0x_10, dtype = float)
nx.append(nx_10)
n0x.append(n0x_10)
nx_11=np.array(nx_11, dtype = float)
n0x_11=np.array(n0x_11, dtype = float)
nx.append(nx_11)
n0x.append(n0x_11)
nx_12=np.array(nx_12, dtype = float)
n0x_12=np.array(n0x_12, dtype = float)
nx.append(nx_12)
n0x.append(n0x_12)
nx_13=np.array(nx_13, dtype = float)
n0x_13=np.array(n0x_13, dtype = float)
nx.append(nx_13)
n0x.append(n0x_13)
nx_14=np.array(nx_14, dtype = float)
n0x_14=np.array(n0x_14, dtype = float)
nx.append(nx_14)
n0x.append(n0x_14)
nx_15=np.array(nx_15, dtype = float)
n0x_15=np.array(n0x_15, dtype = float)
nx.append(nx_15)
n0x.append(n0x_15)
nx_16=np.array(nx_16, dtype = float)
n0x_16=np.array(n0x_16, dtype = float)
nx.append(nx_16)
n0x.append(n0x_16)
nx_17=np.array(nx_17, dtype = float)
n0x_17=np.array(n0x_17, dtype = float)
nx.append(nx_17)
n0x.append(n0x_17)
nx_18=np.array(nx_18, dtype = float)
n0x_18=np.array(n0x_18, dtype = float)
nx.append(nx_18)
n0x.append(n0x_18)
nx_19=np.array(nx_19, dtype = float)
n0x_19=np.array(n0x_19, dtype = float)
nx.append(nx_19)
n0x.append(n0x_19)
nx_20=np.array(nx_20, dtype = float)
n0x_20=np.array(n0x_20, dtype = float)
nx.append(nx_20)
n0x.append(n0x_20)
nx_21=np.array(nx_21, dtype = float)
n0x_21=np.array(n0x_21, dtype = float)
nx.append(nx_21)
n0x.append(n0x_21)
nx_22=np.array(nx_22, dtype = float)
n0x_22=np.array(n0x_22, dtype = float)
nx.append(nx_22)
n0x.append(n0x_22)
nx_23=np.array(nx_23, dtype = float)
n0x_23=np.array(n0x_23, dtype = float)
nx.append(nx_23)
n0x.append(n0x_23)
nx_24=np.array(nx_24, dtype = float)
n0x_24=np.array(n0x_24, dtype = float)
nx.append(nx_24)
n0x.append(n0x_24)
nx_25=np.array(nx_25, dtype = float)
n0x_25=np.array(n0x_25, dtype = float)
nx.append(nx_25)
n0x.append(n0x_25)
nx_26=np.array(nx_26, dtype = float)
n0x_26=np.array(n0x_26, dtype = float)
nx.append(nx_26)
n0x.append(n0x_26)
nx_27=np.array(nx_27, dtype = float)
n0x_27=np.array(n0x_27, dtype = float)
nx.append(nx_27)
n0x.append(n0x_27)
nx_28=np.array(nx_28, dtype = float)
n0x_28=np.array(n0x_28, dtype = float)
nx.append(nx_28)
n0x.append(n0x_28)
nx_29=np.array(nx_29, dtype = float)
n0x_29=np.array(n0x_29, dtype = float)
nx.append(nx_29)
n0x.append(n0x_29)
nx_30=np.array(nx_30, dtype = float)
n0x_30=np.array(n0x_30, dtype = float)
nx.append(nx_30)
n0x.append(n0x_30)
nx_31=np.array(nx_31, dtype = float)
n0x_31=np.array(n0x_31, dtype = float)
nx.append(nx_31)
n0x.append(n0x_31)
nx_32=np.array(nx_32, dtype = float)
n0x_32=np.array(n0x_32, dtype = float)
nx.append(nx_32)
n0x.append(n0x_32)
nx_33=np.array(nx_33, dtype = float)
n0x_33=np.array(n0x_33, dtype = float)
nx.append(nx_33)
n0x.append(n0x_33)
nx_34=np.array(nx_34, dtype = float)
n0x_34=np.array(n0x_34, dtype = float)
nx.append(nx_34)
n0x.append(n0x_34)
nx_35=np.array(nx_35, dtype = float)
n0x_35=np.array(n0x_35, dtype = float)
nx.append(nx_35)
n0x.append(n0x_35)
nx_36=np.array(nx_36, dtype = float)
n0x_36=np.array(n0x_36, dtype = float)
nx.append(nx_36)
n0x.append(n0x_36)
nx_37=np.array(nx_37, dtype = float)
n0x_37=np.array(n0x_37, dtype = float)
nx.append(nx_37)
n0x.append(n0x_37)
nx_38=np.array(nx_38, dtype = float)
n0x_38=np.array(n0x_38, dtype = float)
nx.append(nx_38)
n0x.append(n0x_38)
nx_39=np.array(nx_39, dtype = float)
n0x_39=np.array(n0x_39, dtype = float)
nx.append(nx_39)
n0x.append(n0x_39)
nx_40=np.array(nx_40, dtype = float)
n0x_40=np.array(n0x_40, dtype = float)
nx.append(nx_40)
n0x.append(n0x_40)
nx_41=np.array(nx_41, dtype = float)
n0x_41=np.array(n0x_41, dtype = float)
nx.append(nx_41)
n0x.append(n0x_41)
nx_42=np.array(nx_42, dtype = float)
n0x_42=np.array(n0x_42, dtype = float)
nx.append(nx_42)
n0x.append(n0x_42)
nx_43=np.array(nx_43, dtype = float)
n0x_43=np.array(n0x_43, dtype = float)
nx.append(nx_43)
n0x.append(n0x_43)
nx_44=np.array(nx_44, dtype = float)
n0x_44=np.array(n0x_44, dtype = float)
nx.append(nx_44)
n0x.append(n0x_44)
nx_45=np.array(nx_45, dtype = float)
n0x_45=np.array(n0x_45, dtype = float)
nx.append(nx_45)
n0x.append(n0x_45)
nx_46=np.array(nx_46, dtype = float)
n0x_46=np.array(n0x_46, dtype = float)
nx.append(nx_46)
n0x.append(n0x_46)
nx_47=np.array(nx_47, dtype = float)
n0x_47=np.array(n0x_47, dtype = float)
nx.append(nx_47)
n0x.append(n0x_47)
nx_48=np.array(nx_48, dtype = float)
n0x_48=np.array(n0x_48, dtype = float)
nx.append(nx_48)
n0x.append(n0x_48)
nx_49=np.array(nx_49, dtype = float)
n0x_49=np.array(n0x_49, dtype = float)
nx.append(nx_49)
n0x.append(n0x_49)
nx_50=np.array(nx_50, dtype = float)
n0x_50=np.array(n0x_50, dtype = float)
nx.append(nx_50)
n0x.append(n0x_50)
nx_51=np.array(nx_51, dtype = float)
n0x_51=np.array(n0x_51, dtype = float)
nx.append(nx_51)
n0x.append(n0x_51)
nx_52=np.array(nx_52, dtype = float)
n0x_52=np.array(n0x_52, dtype = float)
nx.append(nx_52)
n0x.append(n0x_52)
nx_53=np.array(nx_53, dtype = float)
n0x_53=np.array(n0x_53, dtype = float)
nx.append(nx_53)
n0x.append(n0x_53)
nx_54=np.array(nx_54, dtype = float)
n0x_54=np.array(n0x_54, dtype = float)
nx.append(nx_54)
n0x.append(n0x_54)
nx_55=np.array(nx_55, dtype = float)
n0x_55=np.array(n0x_55, dtype = float)
nx.append(nx_55)
n0x.append(n0x_55)
nx_56=np.array(nx_56, dtype = float)
n0x_56=np.array(n0x_56, dtype = float)
nx.append(nx_56)
n0x.append(n0x_56)
nx_57=np.array(nx_57, dtype = float)
n0x_57=np.array(n0x_57, dtype = float)
nx.append(nx_57)
n0x.append(n0x_57)
nx_58=np.array(nx_58, dtype = float)
n0x_58=np.array(n0x_58, dtype = float)
nx.append(nx_58)
n0x.append(n0x_58)
nx_59=np.array(nx_59, dtype = float)
n0x_59=np.array(n0x_59, dtype = float)
nx.append(nx_59)
n0x.append(n0x_59)
nx_60=np.array(nx_60, dtype = float)
n0x_60=np.array(n0x_60, dtype = float)
nx.append(nx_60)
n0x.append(n0x_60)
nx_61=np.array(nx_61, dtype = float)
n0x_61=np.array(n0x_61, dtype = float)
nx.append(nx_61)
n0x.append(n0x_61)
nx_62=np.array(nx_62, dtype = float)
n0x_62=np.array(n0x_62, dtype = float)
nx.append(nx_62)
n0x.append(n0x_62)
nx_63=np.array(nx_63, dtype = float)
n0x_63=np.array(n0x_63, dtype = float)
nx.append(nx_63)
n0x.append(n0x_63)
nx_64=np.array(nx_64, dtype = float)
n0x_64=np.array(n0x_64, dtype = float)
nx.append(nx_64)
n0x.append(n0x_64)
nx_65=np.array(nx_65, dtype = float)
n0x_65=np.array(n0x_65, dtype = float)
nx.append(nx_65)
n0x.append(n0x_65)
nx_66=np.array(nx_66, dtype = float)
n0x_66=np.array(n0x_66, dtype = float)
nx.append(nx_66)
n0x.append(n0x_66)
nx_67=np.array(nx_67, dtype = float)
n0x_67=np.array(n0x_67, dtype = float)
nx.append(nx_67)
n0x.append(n0x_67)
nx_68=np.array(nx_68, dtype = float)
n0x_68=np.array(n0x_68, dtype = float)
nx.append(nx_68)
n0x.append(n0x_68)
nx_69=np.array(nx_69, dtype = float)
n0x_69=np.array(n0x_69, dtype = float)
nx.append(nx_69)
n0x.append(n0x_69)
