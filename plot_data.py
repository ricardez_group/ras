# -*- coding: utf-8 -*-
"""
Created on Fri May 24 12:33:12 2024

@author: g2patron
"""

import matplotlib.pyplot as plt

#plots below: note that the first 15 elements of each variable are skipped, these correspond to the initialization stage mentioned in the manuscript, which was not shown

fig,ax = plt.subplots(7,1, figsize=(5,15),dpi=100,sharex=True)

#y[43] is the raw date variable for oxygen concentration
ax[0].plot(time,y[43][15:],'g')
ax[0].set_title('Oxygen concentration in the fish tank')
ax[0].set_ylabel('mg/L')

#y[40] is the raw date variable for ammonia concentration
ax[1].plot(time,y[40][15:],'y')
ax[1].set_title('Ammonia concentration in the fish tank')
ax[1].set_ylabel('mg/L')

#y[53] is the raw date variable for nitrate concentration (units denoted in g/L instead of mg/L as in the paper)
ax[2].plot(time,y[53][15:],'c')
ax[2].set_title('Nitrate concentration in the fish tank')
ax[2].set_ylabel('g/L')

#uu3 is the raw date variable for makeup water flow
ax[3].plot(time,uu3[15:],'b')
ax[3].set_title('Make-up water flowrate')
ax[3].set_ylabel('m3/day')

#uu1 is the raw date variable for oxygen addition to fish tank (units denoted in gO2/(L-day) instead of kg/L as in the paper)
ax[4].plot(time,uu1[15:],'m')
ax[4].set_title('Oxygen addition to the fish tank')
ax[4].set_ylabel('g/ L-day')

#uu2 is the raw date variable for air flow to BR1 (units denoted in gO2/(L-day) instead of m3/L as in the paper)
ax[5].plot(time,uu2[15:],'r')
ax[5].set_title('Air flow to MBBR1')
ax[5].set_ylabel('g/ L-day')

#uu5 is the raw date variable for air flow to BR2 (units denoted in gO2/(L-day) instead of m3/L as in the paper)
ax[6].plot(time,uu5[15:],'k')
ax[6].set_title('Air flow to MBBR2')
ax[6].set_ylabel('g/ L-day')